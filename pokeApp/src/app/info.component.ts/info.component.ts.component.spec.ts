import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoComponentTsComponent } from './info.component.ts.component';

describe('InfoComponentTsComponent', () => {
  let component: InfoComponentTsComponent;
  let fixture: ComponentFixture<InfoComponentTsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InfoComponentTsComponent]
    });
    fixture = TestBed.createComponent(InfoComponentTsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
