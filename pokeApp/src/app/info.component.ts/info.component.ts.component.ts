import { Component,Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-info.component.ts',
  templateUrl: './info.component.ts.component.html',
  styleUrls: ['./info.component.ts.component.css']
})
export class InfoComponentTsComponent {

 constructor(
   public dialogRef: MatDialogRef<InfoComponentTsComponent>,
   @Inject(MAT_DIALOG_DATA) public data: any
 ){
  
 }

 onNoClick(): void {
   this.dialogRef.close();
    }
  
}
