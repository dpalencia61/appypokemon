import { Injectable } from "@angular/core";
import { environment } from "../environmet/environmet";
import { HttpClient } from "@angular/common/http";
import { Observable, throwError, BehaviorSubject  } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class PokemonService{
    [x: string]: any;

    private loggedIn = new BehaviorSubject<boolean>(false);

    baseUrl = environment.baseUrl;
    getDataPokemons: any;

    constructor(private http: HttpClient){}


    get isLogged(): Observable<boolean>{
        return this.loggedIn.asObservable();
    }

    getPokemons(){

        return this.http.get<any>(`${this.baseUrl}persona`)
    }

    createpersona(create:any){
        return this.http.post<any>(`${this.baseUrl}persona`,create)
    }
   
    deletePersona(dpi:number) {
        return this.http.delete<any>(`${this.baseUrl}persona?dpi=${dpi}`);
    }

    updatePersona(dpi: number, datosActualizados: any) {
        return this.http.put<any>(`${this.baseUrl}persona`, datosActualizados);
    }
    info(){
        
    }

    login(credenciales:any){
    return this.http.post<any>(`${this.baseUrl}Usuario/login`,credenciales);
    }

    
    saveToken(token: string): void {
        localStorage.setItem("TOKEN", token);
        this.loggedIn.next(true); // Aquí establecemos loggedIn en true después de guardar el token.
    }

     getToken() {
       return localStorage.getItem("TOKEN");
    }

    deleteToken(){
        this.loggedIn.next(false);
        localStorage.clear();
    }

    exit(){
        
    }
}