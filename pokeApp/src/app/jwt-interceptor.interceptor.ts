import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { PokemonService } from './services/pokemon.service';

@Injectable()
export class JwtInterceptorInterceptor implements HttpInterceptor {

  constructor(private router: Router, private pokemosevice : PokemonService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
   
    let cloneRequest = request;
    console.log(request);

    if (localStorage.getItem('TOKEN')) {
      cloneRequest = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + localStorage.getItem('TOKEN')!
        }
      });
    }
    
    return next.handle(cloneRequest).pipe(
      catchError((error: any) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          // Error 401: No autorizado, redirigir a la página de inicio de sesión
          this.router.navigate(['/login']);
          this.pokemosevice.deleteToken();
        }
        return throwError(error); // Reenviar el error para que otros manejadores lo traten si es necesario
      })
    );
  }
}
