import { Component, Inject, InjectionToken } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
@Component({
  selector: 'app-update-componente',
  templateUrl: './update-componente.component.html',
  styleUrls: ['./update-componente.component.css']
})
export class UpdateComponenteComponent {
    datosActualizados: any;
    dpi:number;
    nombre:string;
    apellido:string;
    edad:number;
    profesion:string;
    sexo:string;


  constructor(
    public dialogRef: MatDialogRef<UpdateComponenteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    ){
      this.datosActualizados = { ...data };
      console.log(data);
      this.nombre = data.nombre;
      this.apellido = data.apellido;
      this.dpi = data.dpi;
      this.edad = data.edad;
      this.profesion = data.profesion;
      this.sexo = data.sexo;
    }

  onNoClick(): void {
  this.dialogRef.close();
    }

  confirmarUpdateElemento() {
    this.datosActualizados = {
      dpi: this.dpi,
      nombre: this.nombre,
      apellido:this.apellido,
      edad:this.edad,
      profesion:this.profesion,
      sexo:this.sexo
    };

    this.dialogRef.close(this.datosActualizados);
    }


    guardarCambios() {

      this.dialogRef.close();
    }

}


    
  


