import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{Routes,RouterModule}from '@angular/router';
import { from } from 'rxjs';
import { PokeDetailComponent } from './components/poke-detail/poke-detail.component';
import { PokeTableComponent } from './componets/poke.table/poke.table.component';
import { LoginComponent } from './auth/login/login.component';
// import { guardGuard } from './auth/guard.guard';


import { checkLoginGuard } from './shared/guards/check-login.guard';

const routes:Routes=[
  {path:'', redirectTo:'/login', pathMatch:'full'},
  {path:'login', component: LoginComponent,
  canActivate: [checkLoginGuard],},  
  {path: 'home', component:PokeTableComponent},
  {path: 'pokeDetail/id', component:PokeDetailComponent},
  {path: '', pathMatch:'full',redirectTo:'home'},
  {path: '**', pathMatch:'full',redirectTo:'home'},
  
  
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports:[RouterModule]
})
export class AppRoutingModule { }
