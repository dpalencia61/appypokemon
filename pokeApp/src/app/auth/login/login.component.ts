import { Component,OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  isLogged : any;

  loginForm = new FormGroup({
    usuario : new FormControl('',Validators.required)
    // Password : new FormControl('',Validators.required)

  })
 
  constructor(private pokemonService: PokemonService, private router: Router){
   
    this.email='';
    this.password='';

  }


  login() {
    if (this.email && this.password) {
      const Credenciales = {
        "user": this.email,
        "password": this.password
      };
      this.pokemonService.login(Credenciales).subscribe(data => {
        console.log(data);
        if (data.success) {
          this.pokemonService.saveToken(data.result);
          // Redirige al usuario a la página "home"
          this.router.navigate(['/home']);
        } else {
          alert(data.message);
        }
      }, error => {
        console.log(error);
      });
    } else {
      alert("Debe llenar los campos");
    }
  }
  
 
  email:string;
  password:string;


  ngOnInit(): void {
    this.pokemonService.isLogged.subscribe((result) => {
      this.isLogged = result; // Aquí asignamos el valor de result a la variable isLogged en tu componente.
      console.log(this.isLogged);
    });
    
    if(this.pokemonService.getToken()){
      this.router.navigate(['/home']);

    }
  
  }

}
