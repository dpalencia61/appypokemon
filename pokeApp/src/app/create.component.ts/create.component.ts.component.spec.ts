import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateComponentTsComponent } from './create.component.ts.component';

describe('CreateComponentTsComponent', () => {
  let component: CreateComponentTsComponent;
  let fixture: ComponentFixture<CreateComponentTsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateComponentTsComponent]
    });
    fixture = TestBed.createComponent(CreateComponentTsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
