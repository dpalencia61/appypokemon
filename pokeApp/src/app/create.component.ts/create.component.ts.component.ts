import { Component, Inject, InjectionToken } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-create.component.ts',
  templateUrl: './create.component.ts.component.html',
  styleUrls: ['./create.component.ts.component.css']
})
export class CreateComponentTsComponent {
   create: any;
   dpi:number;
   nombre:string;
   apellido:string;
   edad:number;
   profesion:string;
   sexo:string;
  
  
  constructor(  public dialogRef: MatDialogRef<CreateComponentTsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any){
      this.nombre = data.nombre;
      this.apellido = data.apellido;
      this.dpi = data.dpi;
      this.edad = data.edad;
      this.profesion = data.profesion;
      this.sexo = data.sexo;
  }
  onNoClick(): void {
    this.dialogRef.close();
      }

  guardar() {
    const obj = {
      dpi: this.dpi,
      nombre:  this.nombre,
      apellido:  this.apellido,
      edad: this. edad,
      profesion:  this.profesion,
      sexo: this.sexo
    }

    this.dialogRef.close(obj);

}



}