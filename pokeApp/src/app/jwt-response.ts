export interface JwtResponseI {
    dataUser: any;
    objUser:{
        id: number,
        name: string,
        email: string,
        // password: string,
        accessToken: string,
        expiresIn: string
    }

}
