import { Component,Inject } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { Router } from '@angular/router';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-mi-componente',
  templateUrl: './mi-componente.component.html',
  styleUrls: ['./mi-componente.component.css'],
  providers: [PokemonService,
  ] 
})
export class MiComponenteComponent {


  constructor(public dialogRef: MatDialogRef<MiComponenteComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private pokemonService: PokemonService,private router: Router

  ){

  }

  onNoClick(): void {
    this.dialogRef.close();
    }


  onLogout(): void {
    this.pokemonService.deleteToken();
    this.router.navigate(['/login']);
  }
}
