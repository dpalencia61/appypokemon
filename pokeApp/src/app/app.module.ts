import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router'; 
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './componets/header/header.component';
import { PokeDetailComponent } from './components/poke-detail/poke-detail.component';
import { PokemonComponent } from './services/pokemon/pokemon.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MaterialModule } from 'material.module';
import { PokeTableComponent } from './componets/poke.table/poke.table.component';
import { PokemonService } from './services/pokemon.service';
import {Component} from '@angular/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatNativeDateModule} from '@angular/material/core';
import { FormsModule } from '@angular/forms';
import { DeleteRowComponent } from './componets/delete.row/delete.row.component';
import { MatDialogModule } from '@angular/material/dialog';
import { UpdateComponenteComponent } from './update-componente/update-componente.component';
import { CreateComponentTsComponent } from './create.component.ts/create.component.ts.component';
import { InfoComponentTsComponent } from './info.component.ts/info.component.ts.component'; 
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { JwtInterceptorInterceptor } from './jwt-interceptor.interceptor';
import { MiComponenteComponent } from './mi-componente/mi-componente.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    PokeDetailComponent,
    PokemonComponent,
    PokeTableComponent,
    DeleteRowComponent,
    UpdateComponenteComponent,
    CreateComponentTsComponent,
    InfoComponentTsComponent,
    LoginComponent,
    RegisterComponent,
    MiComponenteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    RouterModule,
    MatExpansionModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    MatDialogModule
  ],
  providers: [PokemonService,{
    provide: HTTP_INTERCEPTORS,
    useClass: JwtInterceptorInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
