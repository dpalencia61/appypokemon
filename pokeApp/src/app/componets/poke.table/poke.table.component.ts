import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PokemonService } from 'src/app/services/pokemon.service';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { DeleteRowComponent } from '../delete.row/delete.row.component';
import { UpdateComponenteComponent } from 'src/app/update-componente/update-componente.component';
import { CreateComponentTsComponent } from 'src/app/create.component.ts/create.component.ts.component';
import { InfoComponentTsComponent } from 'src/app/info.component.ts/info.component.ts.component';
import { Router } from '@angular/router';
import { MiComponenteComponent } from 'src/app/mi-componente/mi-componente.component';
@Component({
  selector: 'app-poke.table',
  templateUrl: './poke.table.component.html',
  styleUrls: ['./poke.table.component.css']
})
export class PokeTableComponent implements OnInit {
  
  dpi:number;
  nombre:string;
  apellido:string;
  edad:number;
  profesion:string;
  sexo:string;
  getDatapokemons: any;

  displayedColumns: string[] = ['dpi','nombre','apellido','edad','profesion','sexo','borrar','editar'];
  data: any[] = [];
  datasource = new MatTableDataSource<any>(this.data);
  pokemons = [];
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  

  constructor(
    private pokemonService: PokemonService,
    public dialog: MatDialog,private router: Router
    ){
    this.dpi = 0;
    this.nombre = '';
    this.apellido='';
    this.edad=0;
    this.profesion='';
    this.sexo='';
    
    this.pokemonService.getPokemons().subscribe((data:any)=>{
      console.log(data);
      this.nombre = data.nombre;
      this.apellido = data.apellido;
      this.edad = data.edad;
      this.profesion = data.profesion;
      this.sexo = data.sexo;

    });

  }

  ngOnInit():void{
  this.getPokemons();
  // this.getDatapokemons();
  }

  getPokemons(){
    let pokemonData;
      this.pokemonService.getPokemons().subscribe(
        res =>{
        console.log(res);
        pokemonData ={
        nombre: res.nombre,
        apellido: res.apellido,
        dpi: res.dpi,
        sexo: res.sexo,
        edad:res.edad,
        porfesion:res.porfesion
      };

       this.data = res;
       this.datasource = new MatTableDataSource<any>(this.data);
       this.datasource.paginator = this.paginator;
       console.log(this.data);
      
    },
      
   );
 

 }
  

   applyFilter(event: Event) {
   const filterValue = (event.target as HTMLInputElement).value;
   this.datasource.filter = filterValue.trim().toLowerCase();

   if (this.datasource.paginator) {
     this.datasource.paginator.firstPage();
   }

 }


//componente create persona
  createpersona(element:any){
  const dialogRef = this.dialog.open(CreateComponentTsComponent ,{
    width: '500px',
    height: '350px',
    data: element,
      });

        dialogRef.afterClosed().subscribe((create) => {
        if (create) {
          console.log("resultado", create)
          this.guardar(create)
         }
      });
    }//create
    
    guardar(persona: any) {
      
  
      this.pokemonService.createpersona(persona).subscribe(res => {
        console.log(res)
        this.data = res;
         this.datasource = new MatTableDataSource<any>(this.data);
      }, error => {
        console.log("ERROR: ",error)
      });
    }


  borrarElemento(element: any){
    const dialogRef = this.dialog.open(DeleteRowComponent, {
      width: '550px',
      height: '200px',
      data: element,
      panelClass: 'custom-modal-content'
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.confirmarBorrarElemento(element.dpi);
      }
    });
  }

  confirmarBorrarElemento(dpi:number) {
    this.pokemonService.deletePersona(dpi).subscribe(res => {
      this.data = res;
       this.datasource = new MatTableDataSource<any>(this.data);
      }, error => {
      console.log(error);
      });
    }

  //edit 
 // En el componente que llama a UpdateElemento
  UpdateElemento(element: any) {
  const dialogRef = this.dialog.open(UpdateComponenteComponent, {
    width: '500px',
    height: '350px',
    data: element,
    panelClass: 'custom-modal-content'
      });

  dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          console.log("resultado", result)

          this.pokemonService.updatePersona(result.dpi,result).subscribe(res =>{
            this.data = res;
        this.datasource = new MatTableDataSource<any>(this.data);
          },
          error=>{console.log(error)});
        }
      });

    }
    
    info(element: any){
      const dialogRef = this.dialog.open(InfoComponentTsComponent, {
        width: '550px',
        height: '500px',
        data: element,
        panelClass: 'custom-modal-content'
      });
  
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.confirmarBorrarElemento(element.dpi);
        }
      });
    }

    
    exit(element: any){
      const dialogRef = this.dialog.open(MiComponenteComponent,{
        width: '450px',
        height: '200px',
        data: element
      });
      
    }
    
    
    
}

