import { Component, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-delete.row',
  templateUrl: './delete.row.component.html',
  styleUrls: ['./delete.row.component.css']
})
export class DeleteRowComponent {
  dpi:number;
  nombre:string;
  apellido:string;
  edad:number;
  profesion:string;
  sexo:string;

  constructor(
    public dialogRef: MatDialogRef<DeleteRowComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) {
      console.log(data);
    this.nombre = data.nombre;
    this.apellido = data.apellido;
    this.dpi = data.dpi;
    this.edad = data.edad;
    this.profesion = data.profesion;
    this.sexo = data.sexo;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
 
  confirmarBorarElemento() {
    this.dialogRef.close(true);
  }
}
