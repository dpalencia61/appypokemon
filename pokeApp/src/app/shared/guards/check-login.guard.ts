import { Injectable } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { CanActivateFn } from '@angular/router';
import { MiComponenteComponent } from 'src/app/mi-componente/mi-componente.component';
import{
    CanActivate
} from'@angular/router';

import { Observable, from, map, take } from 'rxjs';

  @Injectable({
    providedIn: 'root',
  })
 export class checkLoginGuard implements CanActivate {
    constructor(private PokemonService : PokemonService) {}
    
    canActivate():Observable<boolean>{
        console.log("prueba");
        return this.PokemonService.isLogged.pipe(
            take(1),
            map((isLogged:boolean)=> {
                console.log(isLogged);
                if(this.PokemonService.getToken()){
                    return false
                }
                else{
                    return true
                }
                // return !isLogged
            })
        );
        
    }   
}
